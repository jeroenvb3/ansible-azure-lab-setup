# Ansible Azure Lab Setup Script

## Description
Use Ansible to setup a Lab environment in Azure. It will create a resource group, virtual network, subnet per participant, and an amount of VM's per participant.

## Installation
To use this playbook, you have to install the python module `ansible[azure]`. This can be done by:
```
pip install 'ansible[azure]'
```
this can be done in a virtual enviroment.

Copy `example-vars.yml` to `vars.yml` and change it to the preferred values.


`~/.azure/credentials` should be populated with a service principal, check [Azure docs](https://learn.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli)  for how to create a service principal.

Then populate `~/.azure/credentials` with:
```
[default]
subscription_id=[your azure subscription]
client_id=[client id]
secret=[password of service principal]
tenant=[appid of service principal]
```

## Usage
`ansible-playbook main.yml`.
or for a virtual python environment:
`ansible-playbook main.yml -e 'ansible_python_interpreter=./venv/bin/python3' `.

## Contributing
Feel free to suggest changes, open merge requests, or open issues.

## Authors and acknowledgment
Jeroen van Bennekum

## License
GNU General Public License v3.0
